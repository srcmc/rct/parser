package putils

// A map of added/modified species/names exclusive to Radical Red.
// Based of: https://www.pokeporto.com/pokemon-radical-red-all-pokemon-changes/
var fixedNames = map[string]string{
	"corvknight": "corviknight",  // typo
	"poltegeist": "polteageist",  // typo
	"baraskewda": "barraskewda",  // typo
	"corvsquire": "corvisquire",  // typo
	"crabminble": "crabominable", // typo
	"centskorch": "centiskorch",  // typo
	"stonjorner": "stonjourner",  // typo
	"basclegion": "basculegion",  // typo
	"[2nd":       "honchkrow",    // error in unbound source
}

// Pokemon that should always have a form.
var forcedForm = map[string]struct{}{
	"rotom": {},
}

// All pokemon names containing hyphens ('-').
// Based of: https://bulbapedia.bulbagarden.net/wiki/List_of_Pokémon_by_name
var hyphenNames = map[string]struct{}{
	"ho-oh":     {},
	"porygon-z": {},
	"jangmo-o":  {},
	"hakamo-o":  {},
	"kommo-o":   {},
	"ting-lu":   {},
	"chien-pao": {},
	"wo-chien":  {},
	"chi-yu":    {},
}

// Any forms that where added by Radical Red or spelled differently.
var fixedForms = map[string]string{
	"alola":    "alolan",    // typo
	"hisui":    "hisuian",   // typo
	"galar":    "galarian",  // typo
	"paldea":   "paldean",   // typo
	"valencia": "valencian", // typo
	"mega":     "",          // mega evoltution mid-battle is planned
}

// A map of added/modified moves exclusive to Radical Red 3.02. Inner maps may
// contain pairs of pokemon/moves which are used as replacements. Also supports the
// wildcard pokemon id "*" which denotes a move replacement for all pokemon.
// Based of: https://www.pokeporto.com/pokemon-radical-red-all-pokemon-changes/
var fixedMoves = map[string]map[string]string{
	"aurawheel": {
		"pikachu": "volttackle",
		"*":       "aurawheel",
	}, // Illegal move for pikachu
	"darkhole": {
		"porygon": "shadowball",
		"*":       "darkpulse",
	}, // Radical Red exclusive
	"aquafang": {
		"gyarados": "waterfall",
		"huntail":  "waterfall",
		"*":        "liquidation",
	}, // Radical Red exclusive
	"forbiddenspell": {"*": "mimic"},        // Radical Red exclusive
	"soulrobbery":    {"*": "psystrike"},    // Radical Red exclusive
	"sonicslash":     {"*": "dualwingbeat"}, // Radical Red exclusive
	"dracobarrage":   {"*": "dracometeor"},  // Radical Red exclusive
	// "sappyseed":      {"*": "leechseed"},     // Pokemon Let's Go
	// "zippyzap":       {"*": "zingzap"},       // Pokemon Let's Go
	// "freezyfrost":    {"*": "freezedry"},     // Pokemon Let's Go
	// "sparklyswirl":   {"*": "dazzlinggleam"}, // Pokemon Let's Go
	// "bouncybubble": {
	// 	"milotic": "recover",
	// 	"*":       "scald",
	// }, // Pokemon Let's Go
	// "stoneaxe":       {"*": "stoneedge"},       // Legends: Arceus
	// "ceaselessedge":  {"*": "crunch"},          // Legends: Arceus
	// "infernalparade": {"*": "shadowball"},      // Legends: Arceus
	// "barbbarrage":    {"*": "venoshock"},       // Legends: Arceus
	// "headlongrush":   {"*": "headlongrush"},    // Legends: Arceus
	// "triplearrows":   {"*": "superpower"},      // Legends: Arceus
	// "chloroblast":    {"*": "energyball"},      // Legends: Arceus
	// "victorydance":   {"*": "swordsdance"},     // Legends: Arceus
	// "bittermalice":   {"*": "hex"},             // Legends: Arceus
	// "esperwing":      {"*": "psychic"},         // Legends: Arceus
	"psychicfang":  {"*": "psychicfangs"},    // typo
	"petalstorm":   {"*": "petalblizzard"},   // typo
	"hihorsepower": {"*": "highhorsepower"},  // typo
	"triparrows":   {"*": "triplearrows"},    // typo
	"firstimpress": {"*": "firstimpression"}, // typo
	"badtantrum":   {"*": "stompingtantrum"}, // typo
	"drainkiss":    {"*": "drainingkiss"},    // typo
	"cliffblades":  {"*": "precipiceblades"}, // typo
	"dmaxcannon":   {"*": "dynamaxcannon"},   // typo
	"expandforce":  {"*": "expandingforce"},  // typo
	"scorchsands":  {"*": "scorchingsands"},  // typo
	"spacefury":    {"*": "hyperspacefury"},  // typo
	"spectthief":   {"*": "spectralthief"},   // typo
	"surgestrikes": {"*": "surgingstrikes"},  // typo
	"giantbash":    {"*": "behemothbash"},    // typo
	"clangscales":  {"*": "clangingscales"},  // typo
	"clangsoul":    {"*": "clangoroussoul"},  // typo
	"giantblade":   {"*": "behemothblade"},   // typo
	"dblironbash":  {"*": "doubleironbash"},  // typo
	"spiritlock":   {"*": "spiritshackle"},   // typo
	"tearylook":    {"*": "tearfullook"},     // typo
	"smellingsalt": {"*": "smellingsalts"},   // typo
	"dazzlegleam":  {"*": "dazzlinggleam"},   // typo
	"mistterrain":  {"*": "mistyterrain"},    // typo
	"disarmcry":    {"*": "disarmingvoice"},  // typo
	"trickotreat":  {"*": "trickortreat"},    // typo
	"hijumpkick":   {"*": "highjumpkick"},    // typo
	"waterstar":    {"*": "watershuriken"},   // typo
	"paracharge":   {"*": "paraboliccharge"}, // typo
	"dolleyes":     {"*": "babydolleyes"},    // typo
	"aromamist":    {"*": "aromaticmist"},    // typo
	"dracohammer":  {"*": "dragonhammer"},    // typo
	"darklariat":   {"*": "darkestlariat"},   // typo
	"steampump":    {"*": "steameruption"},   // typo
	"risingvolt":   {"*": "risingvoltage"},   // typo
	"mistyexplode": {"*": "mistyexplosion"},  // typo
	"ceaseedge":    {"*": "ceaselessedge"},   // typo
	"freezeglare":  {"*": "freezingglare"},   // typo
	"sunsteelram":  {"*": "sunsteelstrike"},  // typo
	"infernomarch": {"*": "infernalparade"},  // typo
	"heattwave":    {"*": "heatwave"},        // typo
}

// A map of added/modified abilities exclusive to Radical Red 3.02. Inner maps may
// contain pairs of pokemon/abilities which are used as replacements. Also supports
// the wildcard pokemon id "*" which denotes an ability replacement for all
// pokemon.
// Based of: https://www.pokeporto.com/pokemon-radical-red-all-pokemon-changes/
var fixedAbilities = map[string]map[string]string{
	"blademaster":             {"*": "sharpness"},  // Radical Red exclusive
	"striker":                 {"*": "reckless"},   // Radical Red exclusive
	"felineprowess":           {"*": "cursedbody"}, // Radical Red exclusive
	"flamingsoul":             {"*": "flashfire"},  // Radical Red exclusive
	"sagepower":               {"*": "gluttony"},   // Radical Red exclusive
	"suprise":                 {"*": "hustle"},     // Radical Red exclusive
	"oraoraoraoraragingboxer": {"*": "noguard"},    // Radical Red exclusive
	"badcompany":              {"*": "stamina"},    // Radical Red exclusive
	"parasiticwaste":          {"*": "aftermath"},  // Radical Red exclusive
	"mountaineer":             {"*": "flamebody"},  // Radical Red exclusive
	"bullrush":                {"*": "reckless"},   // Radical Red exclusive
	"primalarmor":             {"*": "pressure"},   // Radical Red exclusive
	"selfsufficient":          {"*": "rockhead"},   // Radical Red exclusive
	"fatalprecision": {
		"eelektross ": "levitate",
		"seviper":     "infiltrator",
		"drampa":      "berserk",
		"heatmore":    "whitesmoke",
		"*":           "adaptility",
	}, // Radical Red exclusive
	"bonezone":       {"*": "battlearmor"},     // Radical Red exclusive
	"blubberdefense": {"*": "multiscale"},      // Radical Red exclusive
	"quillrush":      {"*": "bullrush"},        // Radical Red exclusive
	"blazingsoul":    {"*": "flashfire"},       // Radical Red exclusive
	"neutralizegas":  {"*": "neutralizinggas"}, // typo
	"watercompact":   {"*": "watercompaction"}, // typo
	"asone":          {"*": "unnerve"},         // not in cobblemon?
	"portalpower":    {"*": "magician"},        // unbound exclusive
}

// Any items that are not available in Minecraft/Cobblemon, where added by Radical
// Red or spelled differently (typos) mapped to existing replacements.
var fixedItems = map[string]string{
	"applite":        "eject_pack",            // mega evolution
	"laprasite":      "life_orb",              // mega evolution
	"swampertite":    "leftovers",             // mega evolution
	"ampharosite":    "life_orb",              // mega evolution
	"glalitite":      "leftovers",             // mega evolution
	"metagrossite":   "assault_vest",          // mega evolution
	"manectite":      "expert_belt",           // mega evolution
	"butterfrite":    "expert_belt",           // mega evolution
	"gengarite":      "expert_belt",           // mega evolution
	"alcremite":      "leftovers",             // mega evolution
	"machampite":     "expert_belt",           // mega evolution
	"blastoisnite":   "leftovers",             // mega evolution
	"venusaurite":    "leftovers",             // mega evolution
	"charzarditex":   "life_orb",              // mega evolution
	"charzarditey":   "expert_belt",           // mega evolution
	"charizardite_y": "expert_belt",           // mega evolution
	"mewtwonitex":    "leftovers",             // mega evolution
	"houndoomnite":   "expert_belt",           // mega evolution
	"sceptilite":     "expert_belt",           // mega evolution
	"galladite":      "life_orb",              // mega evolution
	"aerodactlite":   "life_orb",              // mega evolution
	"scizorite":      "life_orb",              // mega evolution
	"altarianite":    "life_orb",              // mega evolution
	"kangaskanite":   "assault_vest",          // mega evolution
	"pidgeotite":     "expert_belt",           // mega evolution
	"pinsirite":      "life_orb",              // mega evolution
	"mawilite":       "expert_belt",           // mega evolution
	"garchompite":    "life_orb",              // mega evolution
	"latiosite":      "life_orb",              // mega evolution
	"copperajite":    "assault_vest",          // mega evolution
	"medichamite":    "choice_scarf",          // mega evolution
	"salamencite":    "life_orb",              // mega evolution
	"aggronite":      "iron_ball",             // mega evolution
	"abomasite":      "assault_vest",          // mega evolution
	"sharpedonite":   "life_orb",              // mega evolution
	"sandacondite":   "life_orb",              // mega evolution
	"sablenite":      "leftovers",             // mega evolution
	"cameruptite":    "assault_vest",          // mega evolution
	"lopunnite":      "life_orb",              // mega evolution
	"toxtricitite":   "assault_vest",          // mega evolution
	"kinglerite":     "life_orb",              // mega evolution
	"blazikenite":    "life_orb",              // mega evolution
	"absolite":       "expert_belt",           // mega evolution
	"heracronite":    "expert_belt",           // mega evolution
	"lucarionite":    "life_orb",              // mega evolution
	"alakazite":      "life_orb",              // mega evolution
	"duraludite":     "assault_vest",          // mega evolution
	"latiasite":      "leftovers",             // mega evolution
	"gyaradosite":    "life_orb",              // mega evolution
	"slowbronite":    "leftovers",             // mega evolution
	"drednawite":     "life_orb",              // mega evolution
	"centiskite":     "life_orb",              // mega evolution
	"beedrillite":    "life_orb",              // mega evolution
	"audinite":       "leftovers",             // mega evolution
	"gardevoirite":   "leftovers",             // mega evolution
	"blue_orb":       "leftovers",             // primal kyogre
	"red_orb":        "leftovers",             // primal groudon
	"adamant_orb":    "leftovers",             // dialga item (boost steel+dragon moves)
	"lustrous_orb":   "leftovers",             // dialga item (boost water+dragon moves)
	"griseous_orb":   "leftovers",             // giratina item (boost ghost+dragon moves)
	"pixie_plate":    "leftovers",             // arceus item (fairy, not in cobblemon)
	"rusty_sword":    "leftovers",             // zacian item (enables behemoth blade)
	"douse_drive":    "leftovers",             // genesect item (water)
	"leek_stick":     "scope_lens",            // 'leek' not in cobblemon (farfetch'd)
	"kommonium_z":    "dragon_gem",            // z-crystals not in cobblemon
	"solganium_z":    "steel_gem",             // z-crystals not in cobblemon
	"mewnium_z":      "psychic_gem",           // z-crystals not in cobblemon
	"psychium_z":     "psychic_gem",           // z-crystals not in cobblemon
	"ghostium_z":     "ghost_gem",             // z-crystals not in cobblemon
	"poisonium_z":    "poison_gem",            // z-crystals not in cobblemon
	"electrium_z":    "electric_gem",          // z-crystals not in cobblemon
	"icium_z":        "ice_gem",               // z-crystals not in cobblemon
	"buginium_z":     "bug_gem",               // z-crystals not in cobblemon
	"waterium_z":     "water_gem",             // z-crystals not in cobblemon
	"groundium_z":    "ground_gem",            // z-crystals not in cobblemon
	"steelium_z":     "steel_gem",             // z-crystals not in cobblemon
	"fightinium_z":   "fighting_gem",          // z-crystals not in cobblemon
	"flyinium_z":     "flying_gem",            // z-crystals not in cobblemon
	"normalium_z":    "normal_gem",            // z-crystals not in cobblemon
	"eevium_z":       "normal_gem",            // z-crystals not in cobblemon
	"stardust":       "",                      // not in cobblemon (no purpose?)
	"green_shard":    "",                      // not in cobblemon (no purpose?)
	"ability_pill":   "ability_capsule",       // typo (no purpose?)
	"nugget":         "minecraft:gold_nugget", // typo (no purpose?)
	"terrain_ext":    "terrain_extender",      // typo
	"weaknesspol":    "weakness_policy",       // typo
	"blackglasses":   "black_glasses",         // typo
	"twistedspoon":   "twisted_spoon",         // typo
	"nevermeltice":   "never_melt_ice",        // typo
	"heavydboots":    "heavy_duty_boots",      // typo
	"charcoal":       "charcoal_stick",        // typo
	"electr_seed":    "electric_seed",         // typo
	"sea_incense":    "mystic_water",          // not in cobblemon
	"steel_memory":   "metal_coat",            // not in cobblemon (sylvally item)
	"decidium_z":     "ghost_gem",             // z-crystals not in cobblemon
	"houndoomite":    "expert_belt",           // mega evolution
	"adrenaline_orb": "mirror_herb",           // not in cobblemon
	"diancite":       "leftovers",             // mega evolution
	"snowball":       "muscle_band",           // not in cobblemon
	"firium_z":       "fire_gem",              // z-crystals not in cobblemon
	"aloraichium_z":  "electric_gem",          // z-crystals not in cobblemon
	"darkinium_z":    "dark_gem",              // z-crystals not in cobblemon
	"kangaskhite":    "leftovers",             // mega evolution
	"mimikium_z":     "fairy_gem",             // z-crystals not in cobblemon
	"tyranitarite":   "leftovers",             // mega evolution
	"banettite":      "life_orb",              // mega evolution
	"lagging_tail":   "iron_ball",             // not in cobblemon
}

// A map containing items that will grant a certain aspect to pokemon (i.e. mega).
