package model

type BattleFormat string

const (
	GEN_9_SINGLES BattleFormat = "GEN_9_SINGLES"
	GEN_9_DOUBLES              = "GEN_9_DOUBLES"
	GEN_9_TRIPLES              = "GEN_9_TRIPLES"
	GEN_9_MULTI                = "GEN_9_MULTI"
	GEN_9_ROYAL                = "GEN_9_ROYAL"
)
