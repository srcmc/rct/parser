package bdsp

import (
	"bufio"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strconv"
	"strings"

	"gitlab.com/srcmc/rct/rcttrp/rcttrp"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/putils"
)

const MISSING_MARKER = "#REF!"
const TEAM_MARKER = "#REF!"

var genders = []string{"GENDERLESS", "MALE", "FEMALE"}
var rng = rand.New(rand.NewSource(0))

type parser struct {
	context *rcttrp.ParseContext
}

func NewParser(context *rcttrp.ParseContext) rcttrp.Parser {
	return &parser{context: context}
}

func (p *parser) ParseFrom(rd io.Reader) chan model.Trainer {
	sc := bufio.NewScanner(rd)
	ch := make(chan model.Trainer)

	go func() {
		p.parseTrainers(sc, ch)
		p.context.NextBaseId()
		close(ch)
	}()

	return ch
}

func (p *parser) parseTrainers(sc *bufio.Scanner, ch chan model.Trainer) {
	// skip inital 3 (header) rows
	for i := 0; i < 3; i++ {
		if !sc.Scan() {
			break
		}
	}

	if sc.Scan() {
		p.parseTrainer(sc, ch)
	}
}

func (p *parser) parseTrainer(sc *bufio.Scanner, ch chan model.Trainer) {
	text := sc.Text()
	row := strings.Split(text, ",")
	row[1] = strings.TrimSpace(row[1])
	var cont bool

	// FOR DEBUGGING
	// dumpRow(row)
	//////////////

	if strings.HasPrefix(row[1], "Trainer ID ") {
		name := strings.TrimSpace(row[3])
		idnum, err := strconv.Atoi(row[1][len("Trainer ID "):])

		if err != nil {
			fmt.Fprintf(os.Stderr, "Skipped row: '%s', %s\n", text, "invalid 'Trainer ID' "+row[1][len("Trainer ID "):])
		} else {
			uid := p.context.NextId(name, idnum)
			var team []model.Pokemon

			if sc.Scan() {
				team, cont = parseTeam(sc, uid)
			}

			if len(team) > 0 && !putils.SkipTrainer(uid) {
				t := model.Trainer{
					Name:     putils.GetDisplayName(uid, name),
					Identity: putils.GetIdentity(uid),
					Team:     team,
				}

				t.SetUid(uid)
				putils.Finalize(&t)
				ch <- t

				if u := p.context.AttemptMerge(&t); u != nil {
					putils.Finalize(u)
					ch <- *u
				}
			} else {
				fmt.Fprintf(os.Stderr, "Skipped trainer: '%s', %s\n", text, "empty team")
			}
		}
	} else {
		fmt.Fprintf(os.Stderr, "Skipped row: '%s', %s\n", text, "missing 'Trainer ID'")
		cont = sc.Scan()
	}

	if cont {
		p.parseTrainer(sc, ch)
	}
}

func parseTeam(sc *bufio.Scanner, trainerId string) ([]model.Pokemon, bool) {
	team := []model.Pokemon{}
	row := strings.Split(sc.Text(), ",")

	for row[0] == TEAM_MARKER {
		// FOR DEBUGGING
		// dumpRow(row)
		//////////////

		pk := parsePokemon(row, trainerId)

		if pk.Species != "" && pk.Ability != "" {
			team = append(team, pk)
		}

		if !sc.Scan() {
			return team, false
		}

		row = strings.Split(sc.Text(), ",")
	}

	return team, true
}

func parsePokemon(row []string, trainerId string) model.Pokemon {
	abilties := []string{}

	if len(row[4]) > 0 && row[4] != MISSING_MARKER {
		abilties = append(abilties, row[4])
	}

	if len(row[5]) > 0 && row[5] != MISSING_MARKER {
		abilties = append(abilties, row[5])
	}

	species, form := putils.GetNameAndForm(row[1])
	species = putils.ToId(species, false)
	form = putils.ToId(form, true)

	gender := putils.GetGender(species).GetRandom()
	level := logerr(strconv.Atoi(row[3])) + putils.GetLevelBuff(trainerId)
	nature := putils.ToId(row[8], true)
	ability := putils.FixAbility(species, putils.ToId(abilties[rng.Int()%len(abilties)], false))
	moves := strings.Split(row[7], "/")
	hpEV := logerr(strconv.Atoi(row[9]))
	atkEV := logerr(strconv.Atoi(row[10]))
	defEV := logerr(strconv.Atoi(row[11]))
	spaEV := logerr(strconv.Atoi(row[12]))
	spdEV := logerr(strconv.Atoi(row[13]))
	speEV := logerr(strconv.Atoi(row[14]))
	hpIV := logerr(strconv.Atoi(row[15]))
	atkIV := logerr(strconv.Atoi(row[16]))
	defIV := logerr(strconv.Atoi(row[17]))
	spaIV := logerr(strconv.Atoi(row[18]))
	spdIV := logerr(strconv.Atoi(row[19]))
	speIV := logerr(strconv.Atoi(row[20]))
	item := putils.FixItem(putils.ToId(row[6], true))
	aspects := []string{}

	if len(form) > 0 {
		aspects = append(aspects, form)
	}

	for i, m := range moves {
		moves[i] = putils.FixMove(species, putils.ToId(m, false))
	}

	return model.Pokemon{
		Species:  species,
		Gender:   gender,
		Level:    level,
		Nature:   nature,
		Ability:  ability,
		Moveset:  moves,
		IVs:      model.Stats{HP: hpIV, Atk: atkIV, Def: defIV, SpA: spaIV, SpD: spdIV, Spe: speIV},
		EVs:      model.Stats{HP: hpEV, Atk: atkEV, Def: defEV, SpA: spaEV, SpD: spdEV, Spe: speEV},
		Shiny:    false,
		HeldItem: item,
		Aspects:  aspects,
	}
}

func logerr[T any](t T, err error) T {
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %s\n", err.Error())
	}

	return t
}

func dumpRow(row []string) {
	drow := make([]string, len(row))

	for i, v := range row {
		drow[i] = "(" + v + ")"
	}

	fmt.Printf("%v\n", drow)
}
