package radicalred

import (
	"bufio"
	"io"
	"strconv"
	"strings"

	"gitlab.com/srcmc/rct/rcttrp/rcttrp"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/putils"
)

const Divider = "===================="

type parser struct {
	context *rcttrp.ParseContext
}

func NewParser(context *rcttrp.ParseContext) rcttrp.Parser {
	return &parser{context: context}
}

func (p *parser) ParseFrom(rd io.Reader) chan model.Trainer {
	sc := bufio.NewScanner(rd)
	ch := make(chan model.Trainer)

	go func() {
		p.parseTrainers(sc, ch)
		p.context.NextBaseId()
		close(ch)
	}()

	return ch
}

func (p *parser) parseTrainers(sc *bufio.Scanner, ch chan model.Trainer) {
	// skip inital 2 empty trainers
	for i := 0; i < 2; i++ {
		for sc.Scan() {
			if sc.Text() == Divider {
				break
			}
		}
	}

	for sc.Scan() {
		if sc.Text() != "" {
			p.parseTrainer(sc, ch)
		}
	}
}

func (p *parser) parseTrainer(sc *bufio.Scanner, ch chan model.Trainer) {
	text := sc.Text()
	name := strings.TrimSpace(strings.ReplaceAll(text[:strings.Index(text, "(")], "{PK}{MN}", ""))
	uid := p.context.NextId(name, int(must(strconv.ParseInt(text[strings.Index(text, "(id:")+5:strings.Index(text, ")")], 0, 32))))
	team := parseTeam(sc, uid)

	if len(team) > 0 && !putils.SkipTrainer(uid) {
		t := model.Trainer{
			Name:     putils.GetDisplayName(uid, name),
			Identity: putils.GetIdentity(uid),
			Team:     team,
		}

		t.SetUid(uid)
		putils.Finalize(&t)
		ch <- t

		if u := p.context.AttemptMerge(&t); u != nil {
			putils.Finalize(u)
			ch <- *u
		}
	}
}

func parseTeam(sc *bufio.Scanner, trainerId string) []model.Pokemon {
	team := []model.Pokemon{}

	for sc.Text() != Divider {
		if !sc.Scan() {
			return team
		}

		if sc.Text() != "" && sc.Text() != Divider {
			pk := parsePokemon(sc, trainerId)

			if pk.Species != "" && pk.Ability != "" {
				team = append(team, pk)
			}
		}
	}

	return team
}

func parsePokemon(sc *bufio.Scanner, trainerId string) model.Pokemon {
	species, form, gender, item := parseSpecies(sc.Text())

	var ability string
	var level int

	evs := []int{0, 0, 0, 0, 0, 0}
	ivs := []int{0, 0, 0, 0, 0, 0}

	for sc.Scan() {
		text := sc.Text()
		propEnd := strings.Index(text, ":")

		if propEnd < 0 {
			break
		}

		if propEnd == 0 {
			continue
		}

		property := text[:propEnd]

		switch property {
		case "Ability":
			abilityI := strings.Index(text, " or")

			if abilityI < 0 {
				ability = putils.FixAbility(species, putils.ToId(text[strings.Index(text, ":")+1:], false))
			} else if putils.GetRNG().Int()%2 == 0 {
				ability = putils.FixAbility(species, putils.ToId(text[strings.Index(text, ":")+1:abilityI], false))
			} else {
				ability = putils.FixAbility(species, putils.ToId(text[abilityI+3:], false))
			}
		case "Level":
			maxLevelIndex := strings.Index(text, "Max Level")

			if maxLevelIndex < 0 {
				level = must(strconv.Atoi(strings.ReplaceAll(text[strings.Index(text, ":")+1:], " ", "")))
				level = level + putils.GetLevelBuff(trainerId)
			} else {
				level = must(strconv.Atoi(strings.ReplaceAll(text[strings.Index(text, "-")+1:], " ", "")))
				level = putils.GetMaxLevel(trainerId) - (putils.GetRNG().Int() % (level + 1))
			}

		case "EVs":
			evs = toStats(strings.Split(strings.TrimSpace(text[strings.Index(text, ":")+1:]), "/"))
		case "IVs":
			ivs = toStats(strings.Split(strings.TrimSpace(text[strings.Index(text, ":")+1:]), "/"))
		}
	}

	text := sc.Text()
	nature := putils.ToId(text[:strings.LastIndex(text, "Nature")], true)
	moves := []string{}

	for sc.Scan() && sc.Text() != "" {
		text = sc.Text()
		mv := putils.FixMove(species, putils.ToId(text[strings.Index(text, "-")+1:], false))

		if mv != "" {
			moves = append(moves, mv)
		}
	}

	aspects := []string{}

	if len(form) > 0 {
		aspects = append(aspects, form)
	}

	if item == "none" {
		item = ""
	}

	return model.Pokemon{
		Species:  species,
		Gender:   gender,
		Level:    level,
		Nature:   nature,
		Ability:  ability,
		Moveset:  moves,
		IVs:      model.Stats{HP: ivs[0], Atk: ivs[1], Def: ivs[2], SpA: ivs[3], SpD: ivs[4], Spe: ivs[5]},
		EVs:      model.Stats{HP: evs[0], Atk: evs[1], Def: evs[2], SpA: evs[3], SpD: evs[4], Spe: evs[5]},
		Shiny:    false,
		HeldItem: item,
		Aspects:  aspects,
	}
}

func parseSpecies(s string) (species, form, gender, item string) {
	nameEnd := strings.Index(s, "(") - 1

	if nameEnd < 0 {
		nameEnd = strings.Index(s, "@") - 1
	}

	species, form = putils.GetNameAndForm(s[:nameEnd])
	genderStart := nameEnd + strings.Index(s[nameEnd:], "(") + 1

	if genderStart > nameEnd {
		gender = s[genderStart : genderStart+1]
		item = s[genderStart+5:]
	} else {
		gender = ""
		item = s[nameEnd+3:]
	}

	return putils.ToId(species, false), putils.ToId(form, true), toGender(gender), putils.FixItem(putils.ToId(item, true))
}
