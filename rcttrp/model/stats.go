package model

type Stats struct {
	HP  int `json:"hp,omitempty"`
	Atk int `json:"atk,omitempty"`
	Def int `json:"def,omitempty"`
	SpA int `json:"spa,omitempty"`
	SpD int `json:"spd,omitempty"`
	Spe int `json:"spe,omitempty"`
}
