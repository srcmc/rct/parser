# RCT - Parser

A command line tool to parse *trainer data* from Pokemon romhacks into trainer configurations for the [CobblemonTrainers mod](https://www.curseforge.com/minecraft/mc-mods/cobblemontrainers) for Minecraft. Designed around and tested with [Pokémon Radical Red 3.02 Trainer Data](dumps/radical_red_3_02_trainers.txt).

## Installation

Requires [Go 1.19](https://go.dev/) or higher:

```bash
go install gitlab.com/srcmc/rct/rcttrp/cmd/rcttrp@latest
```

## Usage

```bash
Usage of rcttrp:
  -out string
        -out <path> (default "out")
  -parse-bdsp value
        -parse-bdsp <url/path>
  -parse-radicalred value
        -parse-radicalred <url/path>
```
