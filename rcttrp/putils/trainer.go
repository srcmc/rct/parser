package putils

import (
	"fmt"
	"strings"

	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
)

type TrainerMerger struct {
	counter int
	buf     map[string]*model.Trainer
}

func NewTrainerMerger() *TrainerMerger {
	return &TrainerMerger{buf: map[string]*model.Trainer{}}
}

func (tm *TrainerMerger) AttemptMerge(t *model.Trainer) *model.Trainer {
	if other, has := tm.buf[t.GetUid()]; has {
		delete(tm.buf, t.GetUid())

		if mf, has := mergefunc[t.GetUid()]; has {
			merged := mf(t, other)
			merged.SetUid(fmt.Sprintf("%s_m%03x", merged.GetUid(), tm.counter))
			tm.counter++
			return merged
		} else if mf, has := mergefunc[other.GetUid()]; has {
			merged := mf(other, t)
			merged.SetUid(fmt.Sprintf("%s_m%03x", merged.GetUid(), tm.counter))
			tm.counter++
			return merged
		}
	} else if with, has := merges[t.GetUid()]; has {
		tm.buf[with] = t
	}

	return nil
}

// GetLevelBuff retrieves a flat value to be added to every member of the team of a
// given trainer (0 by default).
func GetLevelBuff(trainerId string) int {
	if buff, contains := teamBuffs[trainerId]; contains {
		return buff
	}

	return 0
}

// GetMaxLevel retrieves the 'Max Level' replacement for a given trainer, which is
// used to calculate the levels for the trainers pokemon.
func GetMaxLevel(trainerId string) int {
	if maxLevel, has := maxLevels[trainerId]; has {
		return maxLevel
	}

	return defaultMaxLevel
}

// GetDisplayName retrieves a replacement for the display name of the given
// trainer. If no replacement exists backup is returned.
func GetDisplayName(trainerId, backup string) string {
	if name, has := displayNames[trainerId]; has {
		return name
	}

	return backup
}

// GetIdentity retrieves the identity of a trainer or an empty string if no
// identity was defined.
func GetIdentity(trainerId string) string {
	if identity, has := identities[trainerId]; has {
		return identity
	}

	return ""
}

// SkipTrainer reports if a trainer with the given id or id prefix should be
// skipped.
func SkipTrainer(trainerId string) bool {
	if _, has := skips[trainerId]; has {
		return true
	}

	for k := range skips {
		if strings.HasPrefix(trainerId, k) {
			return true
		}
	}

	return false
}

// Applies a predefined model finalizer function to the given trainer model.
func Finalize(t *model.Trainer) {
	tuid := t.GetUid()

	if finalize, has := finalizers[tuid]; has {
		finalize(t)
	} else {
		for uid, finalize := range finalizers {
			if strings.HasPrefix(tuid, uid) {
				finalize(t)
				break
			}
		}
	}
}
