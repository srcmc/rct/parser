package predefined

import (
	"math"

	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
)

func getFor(m map[int]*model.Trainer, t *model.Trainer) *model.Trainer {
	var r *model.Trainer
	min := math.MaxInt
	lvl := 0

	for _, p := range t.Team {
		if p.Level > lvl {
			lvl = p.Level
		}
	}

	for k, v := range m {
		if k < min && lvl <= k {
			min = k
			r = v
		}
	}

	return r
}
