package putils

import (
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model/predefined"
)

// Map of trainer ids to be merged with each other to create a new trainer. Must
// contain an entry for both constellations.
var merges = map[string]string{
	// radicalred
	"rocket_admin_archer_0035": "rocket_admin_ariana_0036", "rocket_admin_ariana_0036": "rocket_admin_archer_0035",
	// bdsp
	"commander_mars_0477": "commander_jupiter_041e", "commander_jupiter_041e": "commander_mars_0477",
	"double_team_jen_0325": "double_team_zac_0324", "double_team_zac_0324": "double_team_jen_0325",
	"double_team_jo_038e": "double_team_pat_038f", "double_team_pat_038f": "double_team_jo_038e",
	"interviewers_oli_03ff": "interviewers_roxy_03fe", "interviewers_roxy_03fe": "interviewers_oli_03ff",
	"ranchers_ava_03bc": "ranchers_matt_03bd", "ranchers_matt_03bd": "ranchers_ava_03bc",
	"ranchers_beth_047c": "ranchers_bob_047d", "ranchers_bob_047d": "ranchers_beth_047c",
	"twins_liv_02f0": "twins_liz_02f1", "twins_liz_02f1": "twins_liv_02f0",
	"twins_emma_03c0": "twins_lil_03c1", "twins_lil_03c1": "twins_emma_03c0",
	"twins_teri_0319": "twins_tia_031a", "twins_tia_031a": "twins_teri_0319",
	"twins_teri_0515": "twins_tia_0516", "twins_tia_0516": "twins_teri_0515",
	"twins_teri_0517": "twins_tia_0518", "twins_tia_0518": "twins_teri_0517",
	"young_couple_mike_047e": "young_couple_nat_047f", "young_couple_nat_047f": "young_couple_mike_047e",
	"young_couple_sue_0316": "young_couple_ty_0315", "young_couple_ty_0315": "young_couple_sue_0316",
}

// Map of trainer merge functions.
var mergefunc = map[string]func(*model.Trainer, *model.Trainer) *model.Trainer{
	// radicalred
	"rocket_admin_archer_0035": func(archer *model.Trainer, arianna *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Rocket Admins Archer & Ariana",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Key(archer).BattleRules,
			Bag:          predefined.Key(archer).Bag,
			Team: []model.Pokemon{
				arianna.Team[0],
				archer.Team[0],
				arianna.Team[1],
				archer.Team[1],
				arianna.Team[2],
				archer.Team[2],
			},
		}

		merged.SetUid("rocket_admin_archer_ariana")
		return merged
	},
	// bdsp
	"commander_mars_0477": func(mars *model.Trainer, jupiter *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Commander Mars & Jupiter",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Key(mars).BattleRules,
			Bag:          predefined.Key(mars).Bag,
			Team: []model.Pokemon{
				mars.Team[0],
				jupiter.Team[0],
			},
		}

		merged.SetUid("commander_mars_jupiter")
		return merged
	},
	"double_team_jen_0325": func(jen *model.Trainer, zac *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Double Team Jen & Zac",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(jen).BattleRules,
			Bag:          predefined.Medium(jen).Bag,
			Team: []model.Pokemon{
				jen.Team[0],
				zac.Team[0],
			},
		}

		merged.SetUid("double_team_jen_zac")
		return merged
	},
	"double_team_jo_038e": func(jo *model.Trainer, pat *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Double Team Jo & Pat",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(jo).BattleRules,
			Bag:          predefined.Medium(jo).Bag,
			Team: []model.Pokemon{
				jo.Team[0],
				pat.Team[0],
			},
		}

		merged.SetUid("double_team_jo_pat")
		return merged
	},
	"interviewers_oli_03ff": func(oli *model.Trainer, roxy *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Interviewers Oli & Roxy",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(oli).BattleRules,
			Bag:          predefined.Medium(oli).Bag,
			Team: []model.Pokemon{
				oli.Team[0],
				roxy.Team[0],
			},
		}

		merged.SetUid("interviewers_oli_roxy")
		return merged
	},
	"ranchers_ava_03bc": func(ava *model.Trainer, matt *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Ranchers Ava & Matt",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(ava).BattleRules,
			Bag:          predefined.Medium(ava).Bag,
			Team: []model.Pokemon{
				ava.Team[0],
				matt.Team[0],
			},
		}

		merged.SetUid("ranchers_ava_matt")
		return merged
	},
	"ranchers_beth_047c": func(beth *model.Trainer, bob *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Ranchers Beth & Bob",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(beth).BattleRules,
			Bag:          predefined.Medium(beth).Bag,
			Team: []model.Pokemon{
				beth.Team[0],
				bob.Team[0],
			},
		}

		merged.SetUid("ranchers_beth_bob")
		return merged
	},
	"twins_liv_02f0": func(liv *model.Trainer, liz *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Twins Liv & Liz",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(liv).BattleRules,
			Bag:          predefined.Medium(liv).Bag,
			Team: []model.Pokemon{
				liv.Team[0],
				liz.Team[0],
			},
		}

		merged.SetUid("twins_liv_liz")
		return merged
	},
	"twins_emma_03c0": func(emma *model.Trainer, lil *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Twins Emma & Lil",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(emma).BattleRules,
			Bag:          predefined.Medium(emma).Bag,
			Team: []model.Pokemon{
				emma.Team[0],
				lil.Team[0],
			},
		}

		merged.SetUid("twins_emma_lil")
		return merged
	},
	"twins_teri_0319": func(teri *model.Trainer, tia *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Twins Teri & Tia",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(teri).BattleRules,
			Bag:          predefined.Medium(teri).Bag,
			Team: []model.Pokemon{
				teri.Team[0],
				tia.Team[0],
			},
		}

		merged.SetUid("twins_teri_tia")
		return merged
	},
	"twins_teri_0515": func(teri *model.Trainer, tia *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Twins Teri & Tia",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(teri).BattleRules,
			Bag:          predefined.Medium(teri).Bag,
			Team: []model.Pokemon{
				teri.Team[0],
				tia.Team[0],
			},
		}

		merged.SetUid("twins_teri_tia")
		return merged
	},
	"twins_teri_0517": func(teri *model.Trainer, tia *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Twins Teri & Tia",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(teri).BattleRules,
			Bag:          predefined.Medium(teri).Bag,
			Team: []model.Pokemon{
				teri.Team[0],
				tia.Team[0],
			},
		}

		merged.SetUid("twins_teri_tia")
		return merged
	},
	"young_couple_mike_047e": func(mike *model.Trainer, nat *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Young Couple Mike & Nat",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(mike).BattleRules,
			Bag:          predefined.Medium(mike).Bag,
			Team: []model.Pokemon{
				mike.Team[0],
				nat.Team[0],
			},
		}

		merged.SetUid("young_couple_mike_nat")
		return merged
	},
	"young_couple_sue_0316": func(sue *model.Trainer, ty *model.Trainer) *model.Trainer {
		merged := &model.Trainer{
			Name:         "Young Couple Sue & Ty",
			BattleFormat: model.GEN_9_DOUBLES,
			BattleRules:  predefined.Medium(sue).BattleRules,
			Bag:          predefined.Medium(sue).Bag,
			Team: []model.Pokemon{
				sue.Team[0],
				ty.Team[0],
			},
		}

		merged.SetUid("young_couple_sue_ty")
		return merged
	},
}
