package predefined

import "gitlab.com/srcmc/rct/rcttrp/rcttrp/model"

func Key(t *model.Trainer) *model.Trainer {
	return getFor(keyTrainers, t)
}

// Map of key trainer templates and max team levels they account for.
var keyTrainers = map[int]*model.Trainer{
	15: {
		Ai: model.RctAI((&model.RctAIConfig{}).WithMaxSelectMargin(0.15)),
		Bag: []model.BagItem{
			{
				Item:     "cobblemon:potion",
				Quantity: 2,
			},
		},
		BattleRules: &model.BattleRules{
			MaxItemUses: 2,
		},
	},
	37: {
		Ai: model.RctAI((&model.RctAIConfig{}).WithMaxSelectMargin(0.15)),
		Bag: []model.BagItem{
			{
				Item:     "cobblemon:super_potion",
				Quantity: 2,
			},
		},
		BattleRules: &model.BattleRules{
			MaxItemUses: 2,
		},
	},
	54: {
		Ai: model.RctAI((&model.RctAIConfig{}).WithMaxSelectMargin(0.15)),
		Bag: []model.BagItem{
			{
				Item:     "cobblemon:hyper_potion",
				Quantity: 2,
			},
		},
		BattleRules: &model.BattleRules{
			MaxItemUses: 2,
		},
	},
	100: {
		Ai: model.RctAI((&model.RctAIConfig{}).WithMaxSelectMargin(0.15)),
		Bag: []model.BagItem{
			{
				Item:     "cobblemon:full_restore",
				Quantity: 2,
			},
		},
		BattleRules: &model.BattleRules{
			MaxItemUses: 2,
		},
	},
}
