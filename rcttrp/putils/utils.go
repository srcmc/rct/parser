package putils

import "math/rand"

type gender_t struct{ male, female, genderless string }

var rng = rand.New(rand.NewSource(0))
var gender = gender_t{"MALE", "FEMALE", "GENDERLESS"}

func GetRNG() *rand.Rand {
	return rng
}

func Gender() *gender_t {
	return &gender
}

func (gt *gender_t) Male() string {
	return gt.male
}

func (gt *gender_t) Female() string {
	return gt.female
}

func (gt *gender_t) Genderless() string {
	return gt.genderless
}
