package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/srcmc/rct/rcttrp/rcttrp"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/parser/bdsp"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/parser/radicalred"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/parser/unbound"
)

const PATH_SEPARATOR = ";"

var outputPath string
var outputFiles map[string]int = map[string]int{}

type sourceFlag struct {
	paths []string
}

func (sf *sourceFlag) String() string {
	return strings.Join(sf.paths, PATH_SEPARATOR)
}

func (sf *sourceFlag) Set(s string) error {
	sf.paths = strings.Split(s, PATH_SEPARATOR)

	for i, path := range sf.paths {
		sf.paths[i] = strings.TrimSpace(path)
	}

	return nil
}

func (sf *sourceFlag) IsUrl(i int) bool {
	if i < len(sf.paths) {
		return strings.HasPrefix(sf.paths[i], "https://")
	}

	return false
}

func main() {
	var radicalredFlag sourceFlag
	var bdspFlag sourceFlag
	var unboundFlag sourceFlag

	flag.Var(&radicalredFlag, "parse-radicalred", "-parse-radicalred \"<urls/paths>\"")
	flag.Var(&bdspFlag, "parse-bdsp", "-parse-bdsp \"<urls/paths>\"")
	flag.Var(&unboundFlag, "parse-unbound", "-parse-unbound \"<urls/paths>\"")
	outFlag := flag.String("out", "out", "-out <path>")
	flag.Parse()

	stat, err := os.Stat(*outFlag)
	onErrExit(err)

	if !stat.IsDir() {
		errExit("output path does not point to a directory")
	}

	outputPath, err = filepath.Abs(*outFlag)
	onErrExit(err)

	context := rcttrp.NewContext()

	if len(radicalredFlag.paths) > 0 {
		runParser(radicalred.NewParser(&context), &radicalredFlag)
	}

	if len(bdspFlag.paths) > 0 {
		runParser(bdsp.NewParser(&context), &bdspFlag)
	}

	if len(unboundFlag.paths) > 0 {
		runParser(unbound.NewParser(&context), &unboundFlag)
	}
}

func runParser(pr rcttrp.Parser, fl *sourceFlag) {
	var ch chan model.Trainer

	for i, path := range fl.paths {
		if fl.IsUrl(i) {
			ch = pr.ParseFrom(fetchCSV(path))
		} else {
			ch = pr.ParseFrom(openFile(path))
		}

		for t := range ch {
			processTrainer(t)
		}
	}
}

func processTrainer(t model.Trainer) {
	js, err := json.MarshalIndent(t, "", "  ")
	onErrExit(err)
	path := filepath.Join(outputPath, t.GetUid()+".json")
	onErrExit(os.WriteFile(path, js, 0666))
	fmt.Println("Created: '" + path + "'")
}

func fetchCSV(url string) io.Reader {
	resp, err := http.Get(url)
	onErrExit(err)
	return resp.Body
}

func openFile(path string) io.Reader {
	f, err := os.Open(path)
	onErrExit(err)
	return f
}

func errExit(msg string) {
	fmt.Fprintf(os.Stderr, "error: %s\n", msg)
	os.Exit(1)
}

func onErrExit(err error) {
	if err != nil {
		errExit(err.Error())
	}
}
