package model

type Pokemon struct {
	Species  string   `json:"species,omitempty"`
	Gender   string   `json:"gender,omitempty"`
	Level    int      `json:"level"`
	Nature   string   `json:"nature,omitempty"`
	Ability  string   `json:"ability,omitempty"`
	Moveset  []string `json:"moveset,omitempty"`
	IVs      Stats    `json:"ivs,omitempty"`
	EVs      Stats    `json:"evs,omitempty"`
	Shiny    bool     `json:"shiny,omitempty"`
	HeldItem string   `json:"heldItem,omitempty"`
	Aspects  []string `json:"aspects,omitempty"`
}
