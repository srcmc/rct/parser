package model

type BattleRules struct {
	MaxItemUses int `json:"maxItemUses,omitempty"`
}
