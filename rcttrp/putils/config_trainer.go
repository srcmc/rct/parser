package putils

// Certain trainers have pokemon in their teams with relative levels (based of the
// 'Max Level' from a player). This value serves as replacement for 'Max Level'.
const defaultMaxLevel = 93

// This map allows to define concrete values to be used as replacement for 'Max
// Level' for certain trainers.
var maxLevels = map[string]int{
	// radicalred
	"trainer_red_0003":          100,
	"professor_oak_00c8":        99,
	"professor_oak_00d2":        99,
	"leader_bugsy_0008":         97,
	"leader_bugsy_004b":         97,
	"leader_morty_001c":         97,
	"leader_morty_0034":         97,
	"leader_misty_0020":         97,
	"leader_clair_0026":         97,
	"leader_falkner_002b":       97,
	"leader_whitney_004c":       97,
	"leader_pryce_004f":         97,
	"leader_chuck_013d":         97,
	"ace_trainer_andrew_0002":   95,
	"ace_trainer_samantha_0052": 95,
	"ace_trainer_nick_0055":     95,
	"ace_trainer_barry_0058":    95,
	"ace_trainer_symes_0071":    95,
	"ace_trainer_runan_007c":    95,
	"ace_trainer_sharon_00ae":   95,
	"ace_trainer_janny_00af":    95,
	"ace_trainer_wilton_0133":   95,
}

// Maps a flat level buff (or malus if negative) to every member within the team of
// a trainer associated by the maps key.
var teamBuffs = map[string]int{
	// "elite_four_agatha_02e1":  15,
	// "elite_four_agatha_019c":  27,
	// "elite_four_bruno_02e0":   17,
	// "elite_four_bruno_019b":   29,
	// "elite_four_lance_02e2":   13,
	// "elite_four_lance_019d":   25,
	// "elite_four_lorelei_02df": 19,
	// "elite_four_lorelei_019a": 31,
	// "champion_terry_02e3": 10,
	// "champion_terry_02e4": 10,
	// "champion_terry_02e5": 10,
	"champion_jax_05b7": 9,
	"trainer_77_05f6":   11,
	"trainer_92_0605":   11,
}

// Alternative display names used for trainers.
var displayNames = map[string]string{
	// radicalred
	"leader_brock_0038":       "Brock",       // Normal
	"leader_bugsy_002f":       "Bugsy",       // Normal
	"leader_bugsy_004b":       "Bugsy",       // Normal
	"leader_bugsy_0008":       "Bugsy",       // Normal
	"leader_chuck_013d":       "Chuck",       // Normal
	"leader_clair_0026":       "Clair",       // Normal
	"leader_erika_0041":       "Erika",       // Normal
	"leader_falkner_002b":     "Falkner",     // Normal
	"leader_falkner_002d":     "Falkner",     // Normal
	"leader_giovanni_015e":    "Giovanni",    // Normal
	"leader_jasmine_001d":     "Jasmine",     // Normal
	"leader_jasmine_003e":     "Jasmine",     // Normal
	"leader_jasmine_003f":     "Jasmine",     // Normal
	"leader_jasmine_0040":     "Jasmine",     // Normal
	"leader_lt_surge_0033":    "Lt. Surge",   // Normal
	"leader_misty_0020":       "Misty",       // Normal
	"leader_misty_0030":       "Misty",       // Normal
	"leader_morty_001c":       "Morty",       // Normal
	"leader_morty_0034":       "Morty",       // Normal
	"leader_pryce_003a":       "Pryce",       // Normal
	"leader_pryce_003b":       "Pryce",       // Normal
	"leader_pryce_003c":       "Pryce",       // Normal
	"leader_pryce_004f":       "Pryce",       // Normal
	"leader_whitney_004c":     "Whitney",     // Normal
	"leader_whitney_0031":     "Whitney",     // Normal
	"elite_four_agatha_02e1":  "Agatha",      // Normal
	"elite_four_agatha_019c":  "Agatha",      // Normal
	"elite_four_bruno_02e0":   "Bruno",       // Normal
	"elite_four_bruno_019b":   "Bruno",       // Normal
	"elite_four_lance_02e2":   "Lance",       // Normal
	"elite_four_lance_019d":   "Lance",       // Normal
	"elite_four_lorelei_02df": "Lorelei",     // Normal
	"elite_four_lorelei_019a": "Lorelei",     // Normal
	"champion_terry_02e3":     "Rival Terry", // Rival
	"champion_terry_02e4":     "Rival Terry", // Rival
	"champion_terry_02e5":     "Rival Terry", // Rival
	"champion_lance_0027":     "Lance",       // Normal
	// bdsp
	"champion_cynthia_05a2":    "Cynthia",  // Normal
	"champion_cynthia_05a7":    "Cynthia",  // Normal
	"elite_four_aaron_05a3":    "Aaron",    // Normal
	"elite_four_aaron_059e":    "Aaron",    // Normal
	"elite_four_bertha_05a4":   "Bertha",   // Normal
	"elite_four_bertha_059f":   "Bertha",   // Normal
	"elite_four_flint_05a0":    "Flint",    // Normal
	"elite_four_flint_05a5":    "Flint",    // Normal
	"elite_four_lucian_05a1":   "Lucian",   // Normal
	"elite_four_lucian_05a6":   "Lucian",   // Normal
	"gym_leader_byron_0590":    "Byron",    // Normal
	"gym_leader_candice_058f":  "Candice",  // Normal
	"gym_leader_fantina_058e":  "Fantina",  // Normal
	"gym_leader_gardenia_058b": "Gardenia", // Normal
	"gym_leader_maylene_058d":  "Maylene",  // Normal
	"gym_leader_roark_058a":    "Roark",    // Normal
	"gym_leader_volkner_0591":  "Volkner",  // Normal
	"gym_leader_wake_058c":     "Wake",     // Normal
	// unbound
	"boss_zeph_1_05bb":                                 "Boss Zeph",
	"boss_zeph_2_05bf":                                 "Boss Zeph",
	"expert_kenlawa_mimikyu_1_05e5":                    "Expert Kenlawa",
	"expert_kenlawa_mimikyu_2_05e6":                    "Expert Kenlawa",
	"expert_kenlawa_mimikyu_3_05e7":                    "Expert Kenlawa",
	"expert_ryuzoji_mimikyu_1_05e2":                    "Expert Ryuzoji",
	"expert_ryuzoji_mimikyu_2_05e3":                    "Expert Ryuzoji",
	"expert_ryuzoji_mimikyu_3_05e4":                    "Expert Ryuzoji",
	"light_of_ruin_admin_ivory_3_05c3":                 "Light of Ruin Admin Ivory",
	"light_of_ruin_aklove_1_05c2":                      "Light of Ruin Leader Aklove",
	"light_of_ruin_aklove_2_05c4":                      "Light of Ruin Leader Aklove",
	"light_of_ruin_aklove_3_05c5":                      "Light of Ruin Leader Aklove",
	"mega_trainer_ravonna_kangaskhan_1_05d3":           "Mega Trainer Ravonna",
	"mega_trainer_ravonna_kangaskhan_2_05d4":           "Mega Trainer Ravonna",
	"mega_trainer_ravonna_kangaskhan_3_05d5":           "Mega Trainer Ravonna",
	"mega_trainer_ravonna_kangaskhan_4_05d6":           "Mega Trainer Ravonna",
	"rival_3_player_chose_beldum_05fb":                 "Rival Wayne",
	"rival_3_player_chose_gible_05fa":                  "Rival Wayne",
	"rival_3_player_chose_larvitar_05fc":               "Rival Wayne",
	"rival_4_player_chose_beldum_05fe":                 "Rival Wayne",
	"rival_4_player_chose_gible_05fd":                  "Rival Wayne",
	"rival_4_player_chose_larvitar_05ff":               "Rival Wayne",
	"rival_5_player_chose_beldum_0601":                 "Rival Wayne",
	"rival_5_player_chose_gible_0600":                  "Rival Wayne",
	"rival_5_player_chose_larvitar_0602":               "Rival Wayne",
	"science_society_scientist_rogue_electivire_05df":  "Science Society Scientist",
	"science_society_scientist_supply_and_demand_05de": "Science Society Scientist",
	"shadow_admin_ivory_1_05ba":                        "Shadow Admin Ivory",
	"shadow_admin_ivory_2_05bc":                        "Shadow Admin Ivory",
	"shadow_admin_marlon_1_05b8":                       "Shadow Admin Marlon",
	"shadow_admin_marlon_2_05be":                       "Shadow Admin Marlon",
	"shadow_admin_marlon_3_05c0":                       "Shadow Admin Marlon",
	"shadow_grunt_w_ivory_2_05bd":                      "Shadow Grunt & Ivory",
	"shadow_grunt_w_marlon_1_05b9":                     "Shadow Grunt & Marlon",
	"sinnoh_leader_candice_rematch_05e9":               "Sinnoh Leader Candice",
	"title_defense_rival_player_chose_beldum_05f0":     "Title Defense Rival Wayne",
	"title_defense_rival_player_chose_gible_05ef":      "Title Defense Rival Wayne",
	"title_defense_rival_player_chose_larvitar_05f1":   "Title Defense Rival Wayne",
	"trainer_77_05f6":                                  "Trainer Blue",
	"trainer_92_0605":                                  "Trainer Blue",
}

// Trainer identities
var identities = map[string]string{
	// radicalred
	"champion_lance_0027":             "Elite Four Lance",
	"champion_terry_01b6":             "Rival Terry",
	"champion_terry_01b7":             "Rival Terry",
	"champion_terry_01b8":             "Rival Terry",
	"elite_four_agatha_02e1":          "Elite Four Agatha",
	"elite_four_agatha_019c":          "Elite Four Agatha",
	"elite_four_bruno_02e0":           "Elite Four Bruno",
	"elite_four_bruno_019b":           "Elite Four Bruno",
	"elite_four_lance_02e2":           "Elite Four Lance",
	"elite_four_lance_019d":           "Elite Four Lance",
	"elite_four_lorelei_02df":         "Elite Four Lorelei",
	"elite_four_lorelei_019a":         "Elite Four Lorelei",
	"leader_brock_0038":               "Leader Brock",
	"leader_clair_0026":               "Leader Clair",
	"leader_erika_0041":               "Leader Erika",
	"leader_giovanni_015e":            "Boss Giovanni",
	"leader_lt_surge_0033":            "Leader Lt. Surge",
	"leader_misty_0020":               "Leader Misty",
	"leader_misty_0030":               "Leader Misty",
	"player_brendan_0200":             "Trainer Brendan",
	"player_may_0201":                 "Trainer May",
	"player_red_0202":                 "Trainer Red",
	"rocket_admin_archer_ariana_m000": "Rocket Admin Archer",
	// bdsp
	"champion_cynthia_05a2":    "Champion Cynthia",
	"champion_cynthia_05a7":    "Champion Cynthia",
	"elite_four_aaron_05a3":    "Elite Four Aaron",
	"elite_four_aaron_059e":    "Elite Four Aaron",
	"elite_four_bertha_05a4":   "Elite Four Bertha",
	"elite_four_bertha_059f":   "Elite Four Bertha",
	"elite_four_flint_05a0":    "Elite Four Flint",
	"elite_four_flint_05a5":    "Elite Four Flint",
	"elite_four_lucian_05a1":   "Elite Four Lucian",
	"elite_four_lucian_05a6":   "Elite Four Lucian",
	"gym_leader_byron_0590":    "Gym Leader Byron",
	"gym_leader_candice_058f":  "Gym Leader Candice",
	"gym_leader_fantina_058e":  "Gym Leader Fantina",
	"gym_leader_gardenia_058b": "Gym Leader Gardenia",
	"gym_leader_maylene_058d":  "Gym Leader Maylene",
	"gym_leader_roark_058a":    "Gym Leader Roark",
	"gym_leader_volkner_0591":  "Gym Leader Volkner",
	"gym_leader_wake_058c":     "Gym Leader Wake",
	// unbound
	"shadow_grunt_w_ivory_2_05bd":                    "Shadow Admin Ivory",
	"shadow_grunt_w_marlon_1_05b9":                   "Shadow Admin Marlon",
	"title_defense_rival_player_chose_beldum_05f0":   "Rival Wayne",
	"title_defense_rival_player_chose_gible_05ef":    "Rival Wayne",
	"title_defense_rival_player_chose_larvitar_05f1": "Rival Wayne",
}

// Ids (and id prefixes) of trainers to skip
var skips = map[string]struct{}{
	// unbound
	"cinder_volcano_":   {}, // boss
	"crystal_peak_":     {}, // boss
	"distortion_world_": {}, // boss
	"ruins_of_void_":    {}, // boss
}
