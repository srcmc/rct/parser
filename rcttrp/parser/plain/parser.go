package text

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/srcmc/rct/rcttrp/rcttrp"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
)

type parser struct {
	context *rcttrp.ParseContext
}

func NewParser(context *rcttrp.ParseContext) rcttrp.Parser {
	return &parser{context: context}
}

func (p *parser) ParseFrom(rd io.Reader) chan model.Trainer {
	ch := make(chan model.Trainer)

	go func() {
		fmt.Println(string(logerr(io.ReadAll(rd))))
		close(ch)
	}()

	return ch
}

func logerr[T any](t T, err error) T {
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %s\n", err.Error())
	}

	return t
}
