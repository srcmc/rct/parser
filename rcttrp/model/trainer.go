package model

type Trainer struct {
	uid          string
	Name         string       `json:"name,omitempty"`
	Identity     string       `json:"identity,omitempty"`
	Ai           AI           `json:"ai,omitempty"`
	BattleFormat BattleFormat `json:"battleFormat,omitempty"`
	BattleRules  *BattleRules `json:"battleRules,omitempty"`
	Bag          []BagItem    `json:"bag,omitempty"`
	Team         []Pokemon    `json:"team,omitempty"`
}

func (t *Trainer) SetUid(uid string) {
	t.uid = uid
}

func (t *Trainer) GetUid() string {
	return t.uid
}
