package predefined

import "gitlab.com/srcmc/rct/rcttrp/rcttrp/model"

func Easy(t *model.Trainer) *model.Trainer {
	return getFor(easyTrainers, t)
}

// Map of easy trainer templates and max team levels they account for.
var easyTrainers = map[int]*model.Trainer{
	100: {
		Ai:  model.RctAI((&model.RctAIConfig{}).WithMaxSelectMargin(0.35)),
		Bag: []model.BagItem{},
	},
}
