package radicalred

import (
	"strconv"
	"strings"
)

func toGender(s string) string {
	s = strings.ToLower(s)

	if s == "f" {
		return "FEMALE"
	}

	if s == "m" {
		return "MALE"
	}

	return "GENDERLESS"
}

func toStats(s []string) []int {
	values := [6]int{}

	for _, stat := range s {
		pair := strings.Split(stat, " ")

		switch pair[1] {
		case "HP":
			values[0], _ = strconv.Atoi(pair[0])
		case "Atk":
			values[1], _ = strconv.Atoi(pair[0])
		case "Def":
			values[2], _ = strconv.Atoi(pair[0])
		case "SpA":
			values[3], _ = strconv.Atoi(pair[0])
		case "SpD":
			values[4], _ = strconv.Atoi(pair[0])
		case "Spe":
			values[5], _ = strconv.Atoi(pair[0])
		}
	}

	return values[:]
}

func must[T any](t T, err error) T {
	if err != nil {
		panic(err)
	}

	return t
}
