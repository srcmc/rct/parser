package rcttrp

import (
	"fmt"
	"io"

	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/putils"
)

type Parser interface {
	// ParseFrom parses trainers from the given reader and returns a channel of the results.
	ParseFrom(io.Reader) chan model.Trainer
}

type ParseContext struct {
	baseId, nextBaseId int
	merger             *putils.TrainerMerger
}

func NewContext() ParseContext {
	return ParseContext{merger: putils.NewTrainerMerger()}
}

func (pc *ParseContext) PeekId(name string, localId int) string {
	return putils.ToId(name, true) + "_" + fmt.Sprintf("%04x", pc.baseId+localId)
}

func (pc *ParseContext) NextId(name string, localId int) string {
	idNum := pc.baseId + localId

	if idNum >= pc.nextBaseId {
		pc.nextBaseId = idNum + 1
	}

	return putils.ToId(name, true) + "_" + fmt.Sprintf("%04x", idNum)
}

func (pc *ParseContext) NextBaseId() {
	pc.baseId = pc.nextBaseId
}

func (pc *ParseContext) AttemptMerge(t *model.Trainer) *model.Trainer {
	return pc.merger.AttemptMerge(t)
}
