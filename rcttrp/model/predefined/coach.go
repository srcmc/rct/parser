package predefined

import "gitlab.com/srcmc/rct/rcttrp/rcttrp/model"

func Coach(t *model.Trainer) *model.Trainer {
	return getFor(coachTrainers, t)
}

// Map of coach trainer templates and max team levels they account for.
var coachTrainers = map[int]*model.Trainer{
	100: {
		Ai:  model.RctAI((&model.RctAIConfig{}).WithItemBias(0).WithSwitchBias(0).WithMoveBias(1).WithStatusMoveBias(1)),
		Bag: []model.BagItem{},
	},
}
