package predefined

import "gitlab.com/srcmc/rct/rcttrp/rcttrp/model"

func Hard(t *model.Trainer) *model.Trainer {
	return getFor(hardTrainers, t)
}

// Map of hard trainer templates and max team levels they account for.
var hardTrainers = map[int]*model.Trainer{
	18: {
		Ai: model.RctAI((&model.RctAIConfig{}).WithMaxSelectMargin(0.15)),
		Bag: []model.BagItem{
			{
				Item:     "cobblemon:remedy",
				Quantity: 1,
			},
		},
		BattleRules: &model.BattleRules{
			MaxItemUses: 4,
		},
	},
	44: {
		Ai: model.RctAI((&model.RctAIConfig{}).WithMaxSelectMargin(0.15)),
		Bag: []model.BagItem{
			{
				Item:     "cobblemon:fine_remedy",
				Quantity: 1,
			},
		},
		BattleRules: &model.BattleRules{
			MaxItemUses: 4,
		},
	},
	100: {
		Ai: model.RctAI((&model.RctAIConfig{}).WithMaxSelectMargin(0.15)),
		Bag: []model.BagItem{
			{
				Item:     "cobblemon:superb_remedy",
				Quantity: 2,
			},
		},
		BattleRules: &model.BattleRules{
			MaxItemUses: 4,
		},
	},
}
