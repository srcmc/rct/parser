package model

type BagItem struct {
	Item     string `json:"item,omitempty"`
	Quantity int    `json:"quantity,omitempty"`
}
