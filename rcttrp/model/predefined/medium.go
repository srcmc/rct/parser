package predefined

import "gitlab.com/srcmc/rct/rcttrp/rcttrp/model"

func Medium(t *model.Trainer) *model.Trainer {
	return getFor(mediumTrainers, t)
}

// Map of medium trainer templates and max team levels they account for.
var mediumTrainers = map[int]*model.Trainer{
	100: {
		Ai:  model.RctAI((&model.RctAIConfig{}).WithMaxSelectMargin(0.25)),
		Bag: []model.BagItem{},
	},
}
