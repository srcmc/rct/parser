package putils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const gqlPokemonEndpoint = "https://graphqlpokemon.favware.tech/v8"
const gqlGetPokemonGender = `{
	getPokemon(pokemon: %s) {
		gender {
			male
			female
		}
	}
}`

// GetNameAndForm takes a radical red pokemon name and returns the (fixed) name and
// form component respectively. If the full name does not define a form the
// returned form string is empty. Takes pokemon with hyphens in their name (like
// Kommo-o) into account.
func GetNameAndForm(full string) (name, form string) {
	full = strings.ToLower(strings.TrimSpace(full))

	if i := strings.Index(full, "("); i >= 0 {
		if j := strings.Index(full[i:], ")"); j > 0 {
			return FixName(full[:i]), FixForm(full[i+1 : i+j])
		}
	}

	if parts := strings.Split(full, " "); len(parts) > 1 {
		for i := 0; i < len(parts); i++ {
			n := FixName(strings.Join(parts[:i+1], " "))

			if _, has := forcedForm[n]; has {
				return n, FixForm(strings.Join(parts[i+1:], " "))
			}

			n = FixName(strings.Join(parts[i+1:], " "))

			if _, has := forcedForm[n]; has {
				return n, FixForm(strings.Join(parts[:i+1], " "))
			}
		}
	}

	for i := len(full); i > 0; i = strings.LastIndex(full[:i], "-") {
		name = full[:i]

		if i+1 < len(full) {
			form = full[i+1:]
		}

		if _, contains := hyphenNames[full[:i]]; contains {
			break
		}
	}

	return FixName(name), FixForm(form)
}

// FixName takes a radical red pokemon name and returns a replacement with any
// potential issues resolved.
func FixName(name string) string {
	if fixedName, contains := fixedNames[name]; contains {
		return fixedName
	}

	return name
}

// FixForm takes a radical red pokemon form and returns a replacement with any
// potential issues resolved.
func FixForm(form string) string {
	if fixedForm, contains := fixedForms[form]; contains {
		return fixedForm
	}

	return form
}

// FixMove takes a radical red pokemon and move and returns a replacement for that
// pokemon with any potential issues resolved.
func FixMove(pokemon, move string) string {
	if replacements, contains := fixedMoves[move]; contains {
		if fixedMove, contains := replacements[pokemon]; contains {
			return fixedMove
		}

		if fixedMove, contains := replacements["*"]; contains {
			return fixedMove
		}

		return ""
	}

	return move
}

// FixAbility takes a radical red pokemon and ability and returns a replacement for
// that pokemon with any potential issues resolved.
func FixAbility(pokemon, ability string) string {
	if replacements, contains := fixedAbilities[ability]; contains {
		if fixedAbility, contains := replacements[pokemon]; contains {
			return fixedAbility
		}

		if fixedAbility, contains := replacements["*"]; contains {
			return fixedAbility
		}

		return ""
	}

	return ability
}

// FixItem retrieves a replacement for the given item id or the item id itself if
// no replacement was defined.
func FixItem(item string) string {
	if repl, has := fixedItems[item]; has {
		return repl
	}

	return item
}

// SpeciesGender represents possible genders of a corresponding pokemon species.
type speciesGender struct {
	rng  *rand.Rand
	Data struct {
		GetPokemon struct {
			Gender struct {
				Male   string
				Female string
			}
		}
	}
}

// Inteface of a species gender type.
type SpeciesGender interface {
	GetMaleChance() float32
	GetFemaleChance() float32
	GetRandom() string
}

// GetMaleChance retrieves the chance for a pokemon of the corresponding species to
// be a male.
func (sg *speciesGender) GetMaleChance() float32 {
	c, _ := strconv.ParseFloat(sg.Data.GetPokemon.Gender.Male[:len(sg.Data.GetPokemon.Gender.Male)-1], 32)
	return float32(c) / 100
}

// GetFemaleChance retrieves the chance for a pokemon of the corresponding species
// to be a female.
func (sg *speciesGender) GetFemaleChance() float32 {
	c, _ := strconv.ParseFloat(sg.Data.GetPokemon.Gender.Female[:len(sg.Data.GetPokemon.Gender.Female)-1], 32)
	return float32(c) / 100
}

// GetRandom retrieves a random possible gender for the corresponding species.
func (sg *speciesGender) GetRandom() string {
	mc, fc := sg.GetMaleChance(), sg.GetFemaleChance()

	if mc == 0 {
		if fc == 0 {
			return Gender().Genderless()
		}

		return Gender().Female()
	} else {
		if fc == 0 || mc > sg.rng.Float32() {
			return Gender().Male()
		}

		return Gender().Female()
	}
}

// GetGenders retrieves possible genders for the given species. The information
// will be requested from https://graphql-pokemon.js.org.
func GetGender(species string) SpeciesGender {
	queryStr, _ := json.Marshal(map[string]string{
		"query": fmt.Sprintf(gqlGetPokemonGender, species),
	})

	if req, err := http.NewRequest("POST", gqlPokemonEndpoint, bytes.NewBuffer(queryStr)); err != nil {
		panic(err) // TODO
	} else {
		req.Header.Add("Content-Type", "application/json")
		client := &http.Client{Timeout: time.Second * 10}

		if resp, err := client.Do(req); err != nil {
			panic(err) // TODO
		} else {
			defer resp.Body.Close()
			data, _ := io.ReadAll(resp.Body)
			sg := speciesGender{rng: GetRNG()}
			json.Unmarshal(data, &sg)
			return &sg
		}
	}
}
