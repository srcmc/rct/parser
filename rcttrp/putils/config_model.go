package putils

import (
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model/predefined"
)

// Maps trainer ids to model finalizer functions. An id may only define a prefix to
// map a finalizer to a trainer group. A full match always takes precedence over a
// partial match.
var finalizers = map[string]func(*model.Trainer){
	"expert_": func(t *model.Trainer) {
		t.Ai = predefined.Hard(t).Ai
		t.Bag = predefined.Hard(t).Bag
		t.BattleRules = predefined.Hard(t).BattleRules
	},
	"friendly_coach_0009": func(t *model.Trainer) {
		t.Name = "Friendly Coach (Spe)"
		t.Ai = predefined.Coach(t).Ai
		t.Bag = predefined.Coach(t).Bag
		t.BattleRules = predefined.Coach(t).BattleRules
	},
	"friendly_coach_000a": func(t *model.Trainer) {
		t.Name = "Friendly Coach (Atk)"
		t.Ai = predefined.Coach(t).Ai
		t.Bag = predefined.Coach(t).Bag
		t.BattleRules = predefined.Coach(t).BattleRules
	},
	"friendly_coach_000b": func(t *model.Trainer) {
		t.Name = "Friendly Coach (Def)"
		t.Ai = predefined.Coach(t).Ai
		t.Bag = predefined.Coach(t).Bag
		t.BattleRules = predefined.Coach(t).BattleRules
	},
	"friendly_coach_000c": func(t *model.Trainer) {
		t.Name = "Friendly Coach (Spa)"
		t.Ai = predefined.Coach(t).Ai
		t.Bag = predefined.Coach(t).Bag
		t.BattleRules = predefined.Coach(t).BattleRules
	},
	"friendly_coach_000d": func(t *model.Trainer) {
		t.Name = "Friendly Coach (Spd)"
		t.Ai = predefined.Coach(t).Ai
		t.Bag = predefined.Coach(t).Bag
		t.BattleRules = predefined.Coach(t).BattleRules
	},
	"friendly_coach_001b": func(t *model.Trainer) {
		t.Name = "Friendly Coach (Hp)"
		t.Ai = predefined.Coach(t).Ai
		t.Bag = predefined.Coach(t).Bag
		t.BattleRules = predefined.Coach(t).BattleRules
	},
	"team_rocket_admin_": func(t *model.Trainer) {
		t.Ai = predefined.Medium(t).Ai
		t.Bag = predefined.Medium(t).Bag
		t.BattleRules = predefined.Medium(t).BattleRules
	},
	"ace_trainer_": func(t *model.Trainer) {
		t.Ai = predefined.Hard(t).Ai
		t.Bag = predefined.Hard(t).Bag
		t.BattleRules = predefined.Hard(t).BattleRules
	},
	"veteran_": func(t *model.Trainer) {
		t.Ai = predefined.Hard(t).Ai
		t.Bag = predefined.Hard(t).Bag
		t.BattleRules = predefined.Hard(t).BattleRules
	},
	"dragon_tamer_": func(t *model.Trainer) {
		t.Ai = predefined.Hard(t).Ai
		t.Bag = predefined.Hard(t).Bag
		t.BattleRules = predefined.Hard(t).BattleRules
	},
	"leader_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"gym_leader_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"boss_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"boss_giovanni_0045": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"team_galactic_boss_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"rocket_admin_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"commander_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"trainer_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"pokemon_trainer_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"game_freaks_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"elite_four_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"elite_four_lorelei_004d": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"elite_four_lorelei_004e": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"champion_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
	},
	"shadow_grunt_w_": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"leader_mirskle_05aa": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"leader_galavan_05af": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"elite_four_moleman_05b3": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"elite_four_penny_06b6": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"champion_jax_05b7": func(t *model.Trainer) {
		t.Ai = predefined.Key(t).Ai
		t.Bag = predefined.Key(t).Bag
		t.BattleRules = predefined.Key(t).BattleRules
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	// doubles (TODO: double check)
	"cool_couple_": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"crush_kin_": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"sis_and_bro_": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"twins_": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_DOUBLES
	},
	"twins_emma_03c0": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"twins_lil_03c1": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"twins_liv_02f0": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"twins_liz_02f1": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"twins_teri_0319": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"twins_teri_0515": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"twins_teri_0517": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"twins_tia_031a": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"twins_tia_0516": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"twins_tia_0518": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_SINGLES
	},
	"young_couple_": func(t *model.Trainer) {
		t.BattleFormat = model.GEN_9_DOUBLES
	},
}
