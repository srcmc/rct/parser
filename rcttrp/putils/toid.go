package putils

import "strings"

func ToId(s string, snake bool) string {
	space := ""

	if snake {
		space = "_"
	}

	rpl := strings.NewReplacer(
		"'", "",
		"’", "",
		"!", "",
		"/", "",
		"[", "",
		"]", "",
		"(", "",
		")", "",
		"{", "",
		"}", "",
		":", "",
		".", "",
		" ", space,
		"-", space,
		"é", "e",
		"♂", "m",
		"♀", "f",
		"&", "and")

	return rpl.Replace(strings.ToLower(strings.TrimSpace(s)))
}
