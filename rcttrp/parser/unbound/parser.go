package unbound

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"gitlab.com/srcmc/rct/rcttrp/rcttrp"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/model"
	"gitlab.com/srcmc/rct/rcttrp/rcttrp/putils"
)

const DIFFICULTY = "Expert" // "Difficult", "Expert", "Insane"

type parser struct {
	context *rcttrp.ParseContext
	idnum   int
}

func NewParser(context *rcttrp.ParseContext) rcttrp.Parser {
	return &parser{context: context}
}

func (p *parser) ParseFrom(rd io.Reader) chan model.Trainer {
	sc := bufio.NewScanner(rd)
	ch := make(chan model.Trainer)

	go func() {
		p.idnum = 0
		p.parseTrainers(sc, ch)
		p.context.NextBaseId()
		close(ch)
	}()

	return ch
}

func (p *parser) parseTrainers(sc *bufio.Scanner, ch chan model.Trainer) {
	if sc.Scan() {
		for p.parseTrainer(sc, ch) {
		}
	}
}

func (p *parser) parseTrainer(sc *bufio.Scanner, ch chan model.Trainer) bool {
	if strings.TrimSpace(sc.Text()) == "" {
		return sc.Scan()
	}

	name, difficulty, species, item := readTrainerHeader(sc.Text())
	altname := name

	if len(name) == 0 {
		altname = generateName(p.idnum)
	}

	uid := p.context.PeekId(altname, p.idnum)
	team, cont := parseTeam(sc, uid, name, difficulty, species, item)
	name = altname

	if difficulty == DIFFICULTY && !putils.SkipTrainer(uid) {
		uid = p.context.NextId(name, p.idnum)

		t := model.Trainer{
			Name:     putils.GetDisplayName(uid, name),
			Identity: putils.GetIdentity(uid),
			Team:     team,
		}

		t.SetUid(uid)
		putils.Finalize(&t)
		ch <- t
		p.idnum++

		if u := p.context.AttemptMerge(&t); u != nil {
			putils.Finalize(u)
			ch <- *u
		}
	}

	return cont
}

func parseTeam(sc *bufio.Scanner, trainerId, trainer, difficulty, species, item string) ([]model.Pokemon, bool) {
	team := []model.Pokemon{}
	nextTrainer := trainer
	nextDifficulty := difficulty

	for nextTrainer == trainer && nextDifficulty == difficulty {
		pk := parsePokemon(sc, trainerId, species, item)
		team = append(team, pk)

		for {
			if sc.Scan() {
				if len(strings.TrimSpace(sc.Text())) > 0 {
					nextTrainer, nextDifficulty, species, item = readTrainerHeader(sc.Text())
					break
				}
			} else {
				return team, false
			}
		}
	}

	return team, true
}

func parsePokemon(sc *bufio.Scanner, trainerId, species, item string) model.Pokemon {
	_species, form := putils.GetNameAndForm(species)
	species = putils.ToId(_species, false)
	form = putils.ToId(form, true)

	sc.Scan()
	level := logerr(strconv.Atoi(strings.Split(sc.Text(), ":")[1][1:])) + putils.GetLevelBuff(trainerId)

	sc.Scan()
	nature := putils.ToId(strings.Split(sc.Text(), " ")[0], true)

	sc.Scan()
	ability := putils.FixAbility(species, putils.ToId(strings.TrimSpace(strings.Split(sc.Text(), ":")[1]), false))

	sc.Scan()

	var hpEV, atkEV, defEV, spaEV, spdEV, speEV int
	hpIV, atkIV, defIV, spaIV, spdIV, speIV := generateIVs(level)

	if strings.HasPrefix(sc.Text(), "EV") {
		if strings.HasPrefix(sc.Text(), "EVs:") {
			stats := strings.Split(strings.Split(sc.Text(), ":")[1], "/")

			for _, s := range stats {
				a := strings.Split(strings.TrimSpace(s), " ")

				switch a[1] {
				case "HP":
					hpEV = logerr(strconv.Atoi(a[0]))
				case "Atk":
					atkEV = logerr(strconv.Atoi(a[0]))
				case "Def":
					defEV = logerr(strconv.Atoi(a[0]))
				case "SpA":
					spaEV = logerr(strconv.Atoi(a[0]))
				case "SpD":
					spdEV = logerr(strconv.Atoi(a[0]))
				case "Spe":
					speEV = logerr(strconv.Atoi(a[0]))
				}
			}
		}

		sc.Scan()
	} else {
		// TODO generate evs?
	}

	if strings.HasPrefix(sc.Text(), "IV") {
		// ivs are always generated
		sc.Scan()
	}

	moves := []string{}
	var move string

	for i := 0; i < 4; i++ {
		if move = strings.TrimSpace(sc.Text()[1:]); len(move) > 0 {
			mv, tp := moveAndHPTyping(putils.FixMove(species, putils.ToId(move, false)))
			moves = append(moves, mv)

			if len(tp) > 0 {
				hpIV, atkIV, defIV, spaIV, spdIV, speIV = generateIVsForHP(level, tp)
			}
		}

		sc.Scan()
	}

	gender := putils.GetGender(species).GetRandom()
	aspects := []string{}

	if len(form) > 0 {
		aspects = append(aspects, form)
	}

	return model.Pokemon{
		Species:  species,
		Gender:   gender,
		Level:    level,
		Nature:   nature,
		Ability:  ability,
		Moveset:  moves,
		IVs:      model.Stats{HP: hpIV, Atk: atkIV, Def: defIV, SpA: spaIV, SpD: spdIV, Spe: speIV},
		EVs:      model.Stats{HP: hpEV, Atk: atkEV, Def: defEV, SpA: spaEV, SpD: spdEV, Spe: speEV},
		Shiny:    false,
		HeldItem: item,
		Aspects:  aspects,
	}
}

func readTrainerHeader(text string) (string, string, string, string) {
	dashI := strings.Index(text, "- ")
	brackOI := strings.LastIndex(text, "(")
	brackCI := strings.LastIndex(text, ")")
	atI := strings.LastIndex(text, "@")

	if dashI < 0 {
		return strings.TrimSpace(text[:brackOI-1]), DIFFICULTY, text[brackOI+1 : brackCI], putils.FixItem(putils.ToId(text[atI+2:], true))
	}

	return strings.TrimSpace(text[:dashI]), strings.TrimSpace(text[dashI+2 : brackOI-1]), text[brackOI+1 : brackCI], putils.FixItem(putils.ToId(text[atI+2:], true))
}

func moveAndHPTyping(move string) (string, string) {
	if strings.HasPrefix(move, "hiddenpower") {
		return "hiddenpower", move[len("hiddenpower"):]
	}

	return move, ""
}

func generateIVsForHP(level int, typing string) (int, int, int, int, int, int) {
	switch typing {
	case "bug":
		return 30, 30, 30, 31, 30, 31
	case "dark":
		return 31, 31, 31, 31, 31, 31
	case "dragon":
		return 30, 31, 31, 31, 31, 31
	case "electric":
		return 31, 31, 31, 30, 31, 31
	case "fighting":
		return 31, 31, 30, 30, 30, 30
	case "fire":
		return 31, 30, 31, 30, 31, 30
	case "flying":
		return 31, 31, 31, 30, 30, 30
	case "ghost":
		return 31, 31, 30, 31, 30, 31
	case "grass":
		return 31, 30, 31, 30, 31, 31
	case "ground":
		return 31, 31, 31, 30, 30, 31
	case "ice":
		return 30, 31, 30, 31, 31, 31
	case "poison":
		return 31, 31, 30, 30, 30, 31
	case "psychic":
		return 30, 31, 31, 31, 31, 30
	case "rock":
		return 31, 31, 30, 31, 30, 30
	case "steel":
		return 31, 31, 31, 31, 30, 31
	case "water":
		return 31, 30, 30, 30, 31, 31
	}

	return generateIVs(level)
}

func generateIVs(level int) (int, int, int, int, int, int) {
	// TODO: based of level or evs?
	return 31, 31, 31, 31, 31, 31
}

func generateName(seed int) string {
	// TODO: some sort of predefined database?
	return fmt.Sprintf("Trainer %d", seed)
}

func logerr[T any](t T, err error) T {
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %s\n", err.Error())
	}

	return t
}
