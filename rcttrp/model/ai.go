package model

type AIModelBuilder struct{}

type RctAIConfig struct {
	MoveBias        *float64 `json:"moveBias,omitempty"`
	StatusMoveBias  *float64 `json:"statusMoveBias,omitempty"`
	SwitchBias      *float64 `json:"switchBias,omitempty"`
	ItemBias        *float64 `json:"itemBias,omitempty"`
	MaxSelectMargin *float64 `json:"maxSelectMargin,omitempty"`
}

func (c *RctAIConfig) WithMoveBias(value float64) *RctAIConfig {
	c.MoveBias = &value
	return c
}

func (c *RctAIConfig) WithStatusMoveBias(value float64) *RctAIConfig {
	c.StatusMoveBias = &value
	return c
}

func (c *RctAIConfig) WithSwitchBias(value float64) *RctAIConfig {
	c.SwitchBias = &value
	return c
}

func (c *RctAIConfig) WithItemBias(value float64) *RctAIConfig {
	c.ItemBias = &value
	return c
}

func (c *RctAIConfig) WithMaxSelectMargin(value float64) *RctAIConfig {
	c.MaxSelectMargin = &value
	return c
}

type Sd5AIConfig struct {
}

type CblAIConfig struct {
	Skill *int `json:"skill,omitempty"`
}

func (c *CblAIConfig) WithSkill(value int) *CblAIConfig {
	c.Skill = &value
	return c
}

type AI interface {
	GetType() string
	GetData() any
}

type ai struct {
	Type string `json:"type,omitempty"`
	Data any    `json:"data,omitempty"`
}

func (a *ai) GetType() string {
	return a.Type
}

func (a *ai) GetData() any {
	return a.Data
}

func RctAI(config *RctAIConfig) AI {
	return &ai{
		Type: "rct",
		Data: config,
	}
}

func Sd5AI(config *Sd5AIConfig) AI {
	return &ai{
		Type: "sd5",
		Data: config,
	}
}

func CblAI(config *CblAIConfig) AI {
	return &ai{
		Type: "cbl",
		Data: config,
	}
}
